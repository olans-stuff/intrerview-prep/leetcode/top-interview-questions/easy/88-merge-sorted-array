class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m -1;  //nums 1 last element
        int j = n -1; //nums 2 last element
        int tracker = m + n -1;
        while (j >= 0) { //if theres still elements in num 2
            if (i >= 0 && nums1[i] > nums2[j]) {
                nums1[tracker--] = nums1[i--];
            } else  {
                nums1[tracker--] = nums2[j--];
            }

        }

    }
}